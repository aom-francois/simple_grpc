#ifndef ASYNC_GRPC_HANDLE_INCLUDED_H 
#define ASYNC_GRPC_HANDLE_INCLUDED_H


namespace async_grpc {
  template<typename ServiceT, typename Req_t>
  void handle(ServiceT* service, Req_t r) {}
}
#endif