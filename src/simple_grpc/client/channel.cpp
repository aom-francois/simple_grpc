#include "simple_grpc/client/channel.h"

namespace simple_grpc {
namespace client { 
  Channel::Channel(grpc_channel* handle, Work_pool* pool)
    : handle_(handle)
    , default_pool_(pool) {}

  Channel::~Channel() {
    if(handle_) {
      grpc_channel_destroy(handle_);
    }
  }

  Channel::Channel(Channel&& rhs) 
    : handle_(rhs.handle_)
    , default_pool_(rhs.default_pool_) {
      rhs.handle_ = nullptr;
  }
  
  Channel& Channel::operator=(Channel&& rhs) {
    handle_ = rhs.handle_;
    default_pool_ = rhs.default_pool_;
    
    rhs.handle_ = nullptr;
    return *this;
  }
}
}
