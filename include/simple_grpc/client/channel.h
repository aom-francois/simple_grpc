#ifndef SIMPLE_GRPC_CLIENT_CHANNEL_INCLUDED_H
#define SIMPLE_GRPC_CLIENT_CHANNEL_INCLUDED_H

#include "grpc/grpc.h"

#include <string>
#include <vector>

namespace simple_grpc {

class Work_pool;

namespace client {
  class Channel {
  public:
    virtual ~Channel();

    Work_pool* default_pool() const { return default_pool_; }

  protected:
    Channel(grpc_channel*, Work_pool*);
    Channel(Channel&& rhs);
    Channel& operator=(Channel&& rhs);

  private:
    grpc_channel* handle_;
    Work_pool* default_pool_;

    Channel(const Channel&) = delete;
    Channel& operator=(const Channel&) = delete;
  };
}
}
#endif