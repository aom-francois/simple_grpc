#ifndef SIMPLE_GRPC_CLIENT_UNSECURE_CHANNEL_INCLUDED_H
#define SIMPLE_GRPC_CLIENT_UNSECURE_CHANNEL_INCLUDED_H

#include "simple_grpc/client/channel.h"

namespace simple_grpc {
namespace client { 
  class Unsecure_channel : public Channel {
  public:
    Unsecure_channel(const std::string& addr, Work_pool* default_pool);
  };
}
}
#endif