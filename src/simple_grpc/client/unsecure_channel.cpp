#include "simple_grpc/client/unsecure_channel.h"

namespace simple_grpc {
namespace client { 
  Unsecure_channel::Unsecure_channel(const std::string& addr, Work_pool* default_pool) 
    : Channel(grpc_insecure_channel_create(addr.c_str(), nullptr, nullptr), default_pool) {

  }
}
}
