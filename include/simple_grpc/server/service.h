#ifndef SIMPLE_GRPC_SERVER_SERVICE_H_INCLUDED
#define SIMPLE_GRPC_SERVER_SERVICE_H_INCLUDED

#include <string>

namespace simple_grpc {

namespace server {
  class Service {
  public:
    virtual ~Service() {}
  };
}


}

#endif