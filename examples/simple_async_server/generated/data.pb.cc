// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: data.proto

#include "data.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// This is a temporary google only hack
#ifdef GOOGLE_PROTOBUF_ENFORCE_UNIQUENESS
#include "third_party/protobuf/version.h"
#endif
// @@protoc_insertion_point(includes)
namespace sas {
class MyRequestDefaultTypeInternal {
 public:
  ::google::protobuf::internal::ExplicitlyConstructed<MyRequest>
      _instance;
} _MyRequest_default_instance_;
class MyResponseDefaultTypeInternal {
 public:
  ::google::protobuf::internal::ExplicitlyConstructed<MyResponse>
      _instance;
} _MyResponse_default_instance_;
}  // namespace sas
namespace protobuf_data_2eproto {
void InitDefaultsMyRequestImpl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#ifdef GOOGLE_PROTOBUF_ENFORCE_UNIQUENESS
  ::google::protobuf::internal::InitProtobufDefaultsForceUnique();
#else
  ::google::protobuf::internal::InitProtobufDefaults();
#endif  // GOOGLE_PROTOBUF_ENFORCE_UNIQUENESS
  {
    void* ptr = &::sas::_MyRequest_default_instance_;
    new (ptr) ::sas::MyRequest();
    ::google::protobuf::internal::OnShutdownDestroyMessage(ptr);
  }
  ::sas::MyRequest::InitAsDefaultInstance();
}

void InitDefaultsMyRequest() {
  static GOOGLE_PROTOBUF_DECLARE_ONCE(once);
  ::google::protobuf::GoogleOnceInit(&once, &InitDefaultsMyRequestImpl);
}

void InitDefaultsMyResponseImpl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#ifdef GOOGLE_PROTOBUF_ENFORCE_UNIQUENESS
  ::google::protobuf::internal::InitProtobufDefaultsForceUnique();
#else
  ::google::protobuf::internal::InitProtobufDefaults();
#endif  // GOOGLE_PROTOBUF_ENFORCE_UNIQUENESS
  {
    void* ptr = &::sas::_MyResponse_default_instance_;
    new (ptr) ::sas::MyResponse();
    ::google::protobuf::internal::OnShutdownDestroyMessage(ptr);
  }
  ::sas::MyResponse::InitAsDefaultInstance();
}

void InitDefaultsMyResponse() {
  static GOOGLE_PROTOBUF_DECLARE_ONCE(once);
  ::google::protobuf::GoogleOnceInit(&once, &InitDefaultsMyResponseImpl);
}

::google::protobuf::Metadata file_level_metadata[2];

const ::google::protobuf::uint32 TableStruct::offsets[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
  ~0u,  // no _has_bits_
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::sas::MyRequest, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  ~0u,  // no _has_bits_
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::sas::MyResponse, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(::sas::MyResponse, data_),
};
static const ::google::protobuf::internal::MigrationSchema schemas[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
  { 0, -1, sizeof(::sas::MyRequest)},
  { 5, -1, sizeof(::sas::MyResponse)},
};

static ::google::protobuf::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::google::protobuf::Message*>(&::sas::_MyRequest_default_instance_),
  reinterpret_cast<const ::google::protobuf::Message*>(&::sas::_MyResponse_default_instance_),
};

void protobuf_AssignDescriptors() {
  AddDescriptors();
  ::google::protobuf::MessageFactory* factory = NULL;
  AssignDescriptors(
      "data.proto", schemas, file_default_instances, TableStruct::offsets, factory,
      file_level_metadata, NULL, NULL);
}

void protobuf_AssignDescriptorsOnce() {
  static GOOGLE_PROTOBUF_DECLARE_ONCE(once);
  ::google::protobuf::GoogleOnceInit(&once, &protobuf_AssignDescriptors);
}

void protobuf_RegisterTypes(const ::std::string&) GOOGLE_PROTOBUF_ATTRIBUTE_COLD;
void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::internal::RegisterAllTypes(file_level_metadata, 2);
}

void AddDescriptorsImpl() {
  InitDefaults();
  static const char descriptor[] GOOGLE_PROTOBUF_ATTRIBUTE_SECTION_VARIABLE(protodesc_cold) = {
      "\n\ndata.proto\022\003sas\"\013\n\tMyRequest\"\032\n\nMyResp"
      "onse\022\014\n\004data\030\001 \001(\t2:\n\tMyService\022-\n\010MyMet"
      "hod\022\016.sas.MyRequest\032\017.sas.MyResponse\"\0002\?"
      "\n\010MyClient\0223\n\016MyClientMethod\022\016.sas.MyReq"
      "uest\032\017.sas.MyResponse\"\0002A\n\tMyClient2\0224\n\017"
      "MyClientMethod2\022\016.sas.MyRequest\032\017.sas.My"
      "Response\"\000b\006proto3"
  };
  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
      descriptor, 258);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "data.proto", &protobuf_RegisterTypes);
}

void AddDescriptors() {
  static GOOGLE_PROTOBUF_DECLARE_ONCE(once);
  ::google::protobuf::GoogleOnceInit(&once, &AddDescriptorsImpl);
}
// Force AddDescriptors() to be called at dynamic initialization time.
struct StaticDescriptorInitializer {
  StaticDescriptorInitializer() {
    AddDescriptors();
  }
} static_descriptor_initializer;
}  // namespace protobuf_data_2eproto
namespace sas {

// ===================================================================

void MyRequest::InitAsDefaultInstance() {
}
#if !defined(_MSC_VER) || _MSC_VER >= 1900
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

MyRequest::MyRequest()
  : ::google::protobuf::Message(), _internal_metadata_(NULL) {
  if (GOOGLE_PREDICT_TRUE(this != internal_default_instance())) {
    ::protobuf_data_2eproto::InitDefaultsMyRequest();
  }
  SharedCtor();
  // @@protoc_insertion_point(constructor:sas.MyRequest)
}
MyRequest::MyRequest(const MyRequest& from)
  : ::google::protobuf::Message(),
      _internal_metadata_(NULL),
      _cached_size_(0) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  // @@protoc_insertion_point(copy_constructor:sas.MyRequest)
}

void MyRequest::SharedCtor() {
  _cached_size_ = 0;
}

MyRequest::~MyRequest() {
  // @@protoc_insertion_point(destructor:sas.MyRequest)
  SharedDtor();
}

void MyRequest::SharedDtor() {
}

void MyRequest::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* MyRequest::descriptor() {
  ::protobuf_data_2eproto::protobuf_AssignDescriptorsOnce();
  return ::protobuf_data_2eproto::file_level_metadata[kIndexInFileMessages].descriptor;
}

const MyRequest& MyRequest::default_instance() {
  ::protobuf_data_2eproto::InitDefaultsMyRequest();
  return *internal_default_instance();
}

MyRequest* MyRequest::New(::google::protobuf::Arena* arena) const {
  MyRequest* n = new MyRequest;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void MyRequest::Clear() {
// @@protoc_insertion_point(message_clear_start:sas.MyRequest)
  ::google::protobuf::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  _internal_metadata_.Clear();
}

bool MyRequest::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  // @@protoc_insertion_point(parse_start:sas.MyRequest)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoffNoLastTag(127u);
    tag = p.first;
    if (!p.second) goto handle_unusual;
  handle_unusual:
    if (tag == 0) {
      goto success;
    }
    DO_(::google::protobuf::internal::WireFormat::SkipField(
          input, tag, _internal_metadata_.mutable_unknown_fields()));
  }
success:
  // @@protoc_insertion_point(parse_success:sas.MyRequest)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:sas.MyRequest)
  return false;
#undef DO_
}

void MyRequest::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:sas.MyRequest)
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()), output);
  }
  // @@protoc_insertion_point(serialize_end:sas.MyRequest)
}

::google::protobuf::uint8* MyRequest::InternalSerializeWithCachedSizesToArray(
    bool deterministic, ::google::protobuf::uint8* target) const {
  (void)deterministic; // Unused
  // @@protoc_insertion_point(serialize_to_array_start:sas.MyRequest)
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()), target);
  }
  // @@protoc_insertion_point(serialize_to_array_end:sas.MyRequest)
  return target;
}

size_t MyRequest::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:sas.MyRequest)
  size_t total_size = 0;

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()));
  }
  int cached_size = ::google::protobuf::internal::ToCachedSize(total_size);
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = cached_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void MyRequest::MergeFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:sas.MyRequest)
  GOOGLE_DCHECK_NE(&from, this);
  const MyRequest* source =
      ::google::protobuf::internal::DynamicCastToGenerated<const MyRequest>(
          &from);
  if (source == NULL) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:sas.MyRequest)
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:sas.MyRequest)
    MergeFrom(*source);
  }
}

void MyRequest::MergeFrom(const MyRequest& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:sas.MyRequest)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

}

void MyRequest::CopyFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:sas.MyRequest)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void MyRequest::CopyFrom(const MyRequest& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:sas.MyRequest)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool MyRequest::IsInitialized() const {
  return true;
}

void MyRequest::Swap(MyRequest* other) {
  if (other == this) return;
  InternalSwap(other);
}
void MyRequest::InternalSwap(MyRequest* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  swap(_cached_size_, other->_cached_size_);
}

::google::protobuf::Metadata MyRequest::GetMetadata() const {
  protobuf_data_2eproto::protobuf_AssignDescriptorsOnce();
  return ::protobuf_data_2eproto::file_level_metadata[kIndexInFileMessages];
}


// ===================================================================

void MyResponse::InitAsDefaultInstance() {
}
#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int MyResponse::kDataFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

MyResponse::MyResponse()
  : ::google::protobuf::Message(), _internal_metadata_(NULL) {
  if (GOOGLE_PREDICT_TRUE(this != internal_default_instance())) {
    ::protobuf_data_2eproto::InitDefaultsMyResponse();
  }
  SharedCtor();
  // @@protoc_insertion_point(constructor:sas.MyResponse)
}
MyResponse::MyResponse(const MyResponse& from)
  : ::google::protobuf::Message(),
      _internal_metadata_(NULL),
      _cached_size_(0) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  data_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  if (from.data().size() > 0) {
    data_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.data_);
  }
  // @@protoc_insertion_point(copy_constructor:sas.MyResponse)
}

void MyResponse::SharedCtor() {
  data_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  _cached_size_ = 0;
}

MyResponse::~MyResponse() {
  // @@protoc_insertion_point(destructor:sas.MyResponse)
  SharedDtor();
}

void MyResponse::SharedDtor() {
  data_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

void MyResponse::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* MyResponse::descriptor() {
  ::protobuf_data_2eproto::protobuf_AssignDescriptorsOnce();
  return ::protobuf_data_2eproto::file_level_metadata[kIndexInFileMessages].descriptor;
}

const MyResponse& MyResponse::default_instance() {
  ::protobuf_data_2eproto::InitDefaultsMyResponse();
  return *internal_default_instance();
}

MyResponse* MyResponse::New(::google::protobuf::Arena* arena) const {
  MyResponse* n = new MyResponse;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void MyResponse::Clear() {
// @@protoc_insertion_point(message_clear_start:sas.MyResponse)
  ::google::protobuf::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  data_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  _internal_metadata_.Clear();
}

bool MyResponse::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  // @@protoc_insertion_point(parse_start:sas.MyResponse)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoffNoLastTag(127u);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // string data = 1;
      case 1: {
        if (static_cast< ::google::protobuf::uint8>(tag) ==
            static_cast< ::google::protobuf::uint8>(10u /* 10 & 0xFF */)) {
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_data()));
          DO_(::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
            this->data().data(), static_cast<int>(this->data().length()),
            ::google::protobuf::internal::WireFormatLite::PARSE,
            "sas.MyResponse.data"));
        } else {
          goto handle_unusual;
        }
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, _internal_metadata_.mutable_unknown_fields()));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:sas.MyResponse)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:sas.MyResponse)
  return false;
#undef DO_
}

void MyResponse::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:sas.MyResponse)
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // string data = 1;
  if (this->data().size() > 0) {
    ::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
      this->data().data(), static_cast<int>(this->data().length()),
      ::google::protobuf::internal::WireFormatLite::SERIALIZE,
      "sas.MyResponse.data");
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      1, this->data(), output);
  }

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()), output);
  }
  // @@protoc_insertion_point(serialize_end:sas.MyResponse)
}

::google::protobuf::uint8* MyResponse::InternalSerializeWithCachedSizesToArray(
    bool deterministic, ::google::protobuf::uint8* target) const {
  (void)deterministic; // Unused
  // @@protoc_insertion_point(serialize_to_array_start:sas.MyResponse)
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // string data = 1;
  if (this->data().size() > 0) {
    ::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
      this->data().data(), static_cast<int>(this->data().length()),
      ::google::protobuf::internal::WireFormatLite::SERIALIZE,
      "sas.MyResponse.data");
    target =
      ::google::protobuf::internal::WireFormatLite::WriteStringToArray(
        1, this->data(), target);
  }

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()), target);
  }
  // @@protoc_insertion_point(serialize_to_array_end:sas.MyResponse)
  return target;
}

size_t MyResponse::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:sas.MyResponse)
  size_t total_size = 0;

  if ((_internal_metadata_.have_unknown_fields() &&  ::google::protobuf::internal::GetProto3PreserveUnknownsDefault())) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        (::google::protobuf::internal::GetProto3PreserveUnknownsDefault()   ? _internal_metadata_.unknown_fields()   : _internal_metadata_.default_instance()));
  }
  // string data = 1;
  if (this->data().size() > 0) {
    total_size += 1 +
      ::google::protobuf::internal::WireFormatLite::StringSize(
        this->data());
  }

  int cached_size = ::google::protobuf::internal::ToCachedSize(total_size);
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = cached_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void MyResponse::MergeFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:sas.MyResponse)
  GOOGLE_DCHECK_NE(&from, this);
  const MyResponse* source =
      ::google::protobuf::internal::DynamicCastToGenerated<const MyResponse>(
          &from);
  if (source == NULL) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:sas.MyResponse)
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:sas.MyResponse)
    MergeFrom(*source);
  }
}

void MyResponse::MergeFrom(const MyResponse& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:sas.MyResponse)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::google::protobuf::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  if (from.data().size() > 0) {

    data_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.data_);
  }
}

void MyResponse::CopyFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:sas.MyResponse)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void MyResponse::CopyFrom(const MyResponse& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:sas.MyResponse)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool MyResponse::IsInitialized() const {
  return true;
}

void MyResponse::Swap(MyResponse* other) {
  if (other == this) return;
  InternalSwap(other);
}
void MyResponse::InternalSwap(MyResponse* other) {
  using std::swap;
  data_.Swap(&other->data_);
  _internal_metadata_.Swap(&other->_internal_metadata_);
  swap(_cached_size_, other->_cached_size_);
}

::google::protobuf::Metadata MyResponse::GetMetadata() const {
  protobuf_data_2eproto::protobuf_AssignDescriptorsOnce();
  return ::protobuf_data_2eproto::file_level_metadata[kIndexInFileMessages];
}


// @@protoc_insertion_point(namespace_scope)
}  // namespace sas

// @@protoc_insertion_point(global_scope)
