#ifndef SIMPLE_GRPC_FUTURE_INCLUDED_H 
#define SIMPLE_GRPC_FUTURE_INCLUDED_H

#include <condition_variable>
#include <mutex>

namespace simple_grpc {
  template<typename T>
  class Future {
  public:

    Future() = default;
    Future(Future&&) = default;
    Future(const Future&) = delete;
    Future& operator=(Future&&) = default;
    Future& operator=(const Future&) = delete;
    
    // Synchronously calls cb once the future has been fulfilled.
    // cb will be invoked directly in whichever thread fullfills
    // the future.
    //
    // returns a future of whichever type is returned by cb.
    // If this future is failed, then the resulting future
    // will be failed with that same failure, and cb will be destroyed without being invoked.
    template<typename CbT>
    auto then(CbT cb) {}

    template<typename CbT>
    auto then_expected(CbT cb) {}

    // Queues cb in the target queue once the future has been fulfilled.
    //
    // It's expected that the queue itself will outlive the future.
    //
    // Returns a future of whichever type is returned by cb.
    // If this future is failed, then the resulting future
    // will be failed with that same failure, and cb will be destroyed without being invoked.
    //
    // The assignment of the failure will be done synchronously in the fullfilling thread.
    // TODO: Maybe we can add an option to change that behavior
    template<typename CbT, typename QueueT>
    auto then(CbT cb, QueueT& queue) {}

    // Effectively eqivalent to std::future::get()
    T get() { 
        T result;
        bool done = false;

        std::condition_variable condition;
        std::mutex mtx;

        std::unique_lock lock(mtx);

        this->then_expected([&](T v){
            std::lock_guard guard(mtx);
            result = std::move(v);
            done = true;
            condition.notify_one();
        });

        condition.wait(lock, [&]{ return done;});

        return result;
    }
  };
}
#endif