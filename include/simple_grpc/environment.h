#ifndef SIMPLE_GRPC_ENVIRONMENT_H_INCLUDED
#define SIMPLE_GRPC_ENVIRONMENT_H_INCLUDED

#include "grpc/grpc.h"

#include "simple_grpc/config.h"

#include <cassert>

namespace simple_grpc {

class [[nodiscard]] Environment {
  Environment(const Environment&) = delete;
  Environment& operator=(const Environment&) = delete;

public:
  Environment();
  Environment(Environment&&);
  Environment& operator=(Environment&&);
  
  ~Environment();

  static void assert_valid() {
    if constexpr (simple_grpc_validation_enabled) {
      assert(is_valid());
    }
  }

  static bool is_valid();
private:

};

}

#endif