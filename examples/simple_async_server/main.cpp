#include "simple_grpc/simple_grpc.h"
#include "generated/data.sgrpc.pb.h"

#include <thread>
#include <chrono>


namespace rpc = simple_grpc;
namespace srv = rpc::server;
namespace client = rpc::client;

using rpc::Future;

/*
class MyService_impl : public ::sas::sup::MyService {
public: 
  Future<::sas::sup::MyResponse> MyMethod(::sas::sup::MyRequest req) override {
    return {}
  }
};
*/

int main() {
  rpc::Environment grpc_env;
  
  rpc::Work_pool wp(1, 1);
  client::Unsecure_channel channel("localhost:12345", &wp);

  sas::MyService::Stub stub(&channel);


  auto answer = stub.MyMethod({}).get();

  std::cout << answer.data();

  return 0;
}