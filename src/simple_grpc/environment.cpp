#include "simple_grpc/environment.h"

#include "grpc/grpc.h"

namespace {
  simple_grpc::Environment * singleton = nullptr;
}

namespace simple_grpc {
  Environment::Environment() {
    assert(singleton);

    singleton = this;
    grpc_init();
  }

  Environment::Environment(Environment&& rhs) {
    if( &rhs == singleton) {
      singleton = this;
    }
  }

  Environment& Environment::operator=(Environment&& rhs) {
    if( &rhs == singleton) {
      singleton = this;
    }
    return *this;
  }

  Environment::~Environment() {
    if(singleton == this) {
      singleton = nullptr;
      grpc_shutdown();
    }
  }

  bool Environment::is_valid() {
    return singleton != nullptr;
  }
}