#ifndef SIMPLE_GRPC_SERVER_SERVER_H_INCLUDED
#define SIMPLE_GRPC_SERVER_SERVER_H_INCLUDED

#include "simple_grpc/server/credentials.h"

#include "grpc/grpc.h"

#include <memory>
#include <string>
#include <vector>

namespace simple_grpc {

namespace server {

  class Service;

  class Config {
  public:
    Config& with_service(Service* service);
    Config& with_listening_port(std::string addr, std::shared_ptr<Credentials> creds ={});

  private:
    struct Port {
      std::string addr;
      std::shared_ptr<Credentials> creds;
    };

    std::vector<Service*> services_;
    std::vector<Port> ports_;
  };

  class Server {
    grpc_server * impl_ = nullptr;

  public:
    Server(Config cfg);
    Server(Server&& rhs);
    Server& operator=(Server&& rhs);
    ~Server();

  private:

    void cleanup_();

    // Noncopyable
    Server(const Server&) = delete;
    Server& operator=(const Server&) = delete;
  };
}


}

#endif