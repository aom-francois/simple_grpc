#ifndef SIMPLE_GRPC_COMPLETION_QUEUE_INCLUDED_H 
#define SIMPLE_GRPC_COMPLETION_QUEUE_INCLUDED_H

#include "grpc/grpc.h"

namespace simple_grpc {
  class Completion_queue {
    public:

    private:
      grpc_completion_queue* handle_;
  };
}
#endif