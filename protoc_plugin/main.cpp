#include <google/protobuf/compiler/code_generator.h>
#include <google/protobuf/compiler/plugin.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream.h>

#include <filesystem>
#include <functional>

using google::protobuf::Descriptor;
using google::protobuf::FileDescriptor;
using google::protobuf::ServiceDescriptor;

using google::protobuf::compiler::CodeGenerator;
using google::protobuf::compiler::GeneratorContext;
using google::protobuf::io::ZeroCopyOutputStream;
using google::protobuf::io::CodedOutputStream;

using std::filesystem::path;

const char * header_extension = ".sgrpc.pb.h";
const char * source_extension = ".sgrpc.pb.cc";
const char * pb_header_extension = ".pb.h";

std::string class_name(const Descriptor* desc) {
  auto outer = desc;
  while (outer->containing_type()) {
    outer = outer->containing_type();
  }

  auto outer_name = outer->full_name();
  auto inner_name = desc->full_name().substr(outer_name.size());

  std::ostringstream result;
  result << "::";

  for(auto& c: outer_name) {
    if(c == '.') {
      result << "::";
    }
    else {
      result << c;
    }
  }

  for(auto& c: inner_name) {
    if(c == '.') {
      result << "_";
    }
    else {
      result << c;
    }
  }

  return result.str();
}

std::string header_guard(std::string_view file_name) {
  std::ostringstream result;
  for(auto c: file_name) {
    if(std::isalnum(c)) {
      result << c;
    }
    else {
      result << std::hex << int(c);
    }
  }
  return result.str();
}

std::vector<std::string> tokenize(const std::string& str, char c) {
  std::vector<std::string> result;
  if(!str.empty()) {
    auto current = 0;
    auto next = str.find(c);

    while(next != std::string::npos) {
      result.push_back(str.substr(current, next));
      current = next + 1;
      next = str.find(c, current);
    }

    if(current != next) {
      result.push_back(str.substr(current, next));
    }
  }

  return result;
}

std::vector<std::string> package_parts(const FileDescriptor* file) {
  return tokenize(file->package(), '.');
}

void generate_service_header(const ServiceDescriptor* service, std::ostream& dst) {
  dst << "class " << service->name() << " : public ::simple_grpc::server::Service {\n"
      << "public:\n"
      << "  " << service->name() << "();\n"
      << "  virtual ~" << service->name() << "() {}\n\n";

  for(int i = 0 ; i < service->method_count(); ++i) {
    auto method = service->method(i);

    auto input = method->input_type();
    auto output = method->output_type();


    dst << "  virtual ::simple_grpc::Future<"<< class_name(output) << "> " << method->name() << "("<< class_name(input) << ") = 0;\n";
  }
  dst << "\n";

  dst << "  class Stub_interface {\n"
      << "  public:\n"
      << "    virtual ~Stub_interface() {}\n\n";

  for(int i = 0 ; i < service->method_count(); ++i) {
    auto method = service->method(i);

    auto input = method->input_type();
    auto output = method->output_type();

    dst << "    virtual ::simple_grpc::Future<"<< class_name(output) << "> " << method->name() << "("<< class_name(input) << ") = 0;\n";
  }
  dst << "  };\n\n";
  dst << "  class Stub final : public Stub_interface {\n"
      << "  public:\n"
      << "    Stub(::simple_grpc::client::Channel*, ::simple_grpc::Work_pool* = nullptr);\n\n";

  for(int i = 0 ; i < service->method_count(); ++i) {
    auto method = service->method(i);

    auto input = method->input_type();
    auto output = method->output_type();

    dst << "    ::simple_grpc::Future<"<< class_name(output) << "> " << method->name() << "("<< class_name(input) << ") override;\n";
  }

  dst << "  private:\n"
      << "    ::simple_grpc::client::Channel* channel_;\n"
      << "    ::simple_grpc::Work_pool* default_queue_;\n"
      << "  };\n";

  dst << "};\n\n";
}

void generate_service_source(const ServiceDescriptor* service, std::ostream& dst) {
  auto name = service->name();

  dst << "// ********** " << name << " ********** //\n\n";

  dst << name << "::Stub::Stub(::simple_grpc::client::Channel* c, ::simple_grpc::Work_pool* default_pool)\n"
      << "  : channel_(c), default_queue_(default_pool ? default_pool : c->default_pool()) {}\n\n";

  for(int i = 0 ; i < service->method_count(); ++i) {
    auto method = service->method(i);

    auto input = method->input_type();
    auto output = method->output_type();

    dst << "// " << method->name() << "\n";
    dst << "::simple_grpc::Future<"<< class_name(output) << "> " 
      << name << "::Stub::" << method->name() << "("<< class_name(input) << ") {\n"
      << "  return {};\n"
      << "};\n\n";
  }

  dst << "\n";
}


std::string generate_header(const FileDescriptor* file) {
  
  std::ostringstream result;

  auto file_name = path(file->name()).stem().string();

  // Prologue
  result 
    << "// This code was generated by the simple_grpc protoc plugin.\n"
    << "#ifndef SIMPLE_GRPC_" << header_guard(file_name) << "_INCLUDED_H\n"
    << "#define SIMPLE_GRPC_" << header_guard(file_name) << "_INCLUDED_H\n"
    << "\n";

  // Headers
  result << "#include \"" << file_name << pb_header_extension << "\"" << "\n\n";

  result << "#include \"simple_grpc/gen_support.h\"" << "\n\n";

  result << "#include <memory>\n\n";

  // namespace
  auto package = package_parts(file);

  if (!package.empty()) {
    for (const auto& p : package) {
      result << "namespace " << p << " {\n";
    }
    result << "\n";
  }


  // Services
  for (int i = 0; i < file->service_count(); ++i) {
    generate_service_header(file->service(i), result);
  }

  // Epilogue
  if (!package.empty()) {
    for (const auto& p : package) {
      result << "} // namespace" << p << "\n";
    }
    result << "\n";
  }

  result << "#endif\n";

  return result.str();
}


std::string generate_source(const FileDescriptor* file) {
  std::ostringstream result;

  auto file_name = path(file->name()).stem().string();

  // Prologue
  result 
    << "// This code was generated by the simple_grpc protoc plugin.\n\n";


  // Headers
  result << "#include \"" << file_name << header_extension << "\"" << "\n\n";

  // namespace
  auto package = package_parts(file);

  if (!package.empty()) {
    for (const auto& p : package) {
      result << "namespace " << p << " {\n";
    }
    result << "\n";
  }


  // Services
  for (int i = 0; i < file->service_count(); ++i) {
    generate_service_source(file->service(i), result);
  }

  // Epilogue
  if (!package.empty()) {
    for (const auto& p : package) {
      result << "} // namespace" << p << "\n";
    }
    result << "\n";
  }

  return result.str();
}

class Generator : public CodeGenerator {
public:
  // Generates code for the given proto file, generating one or more files in
  // the given output directory.
  //
  // A parameter to be passed to the generator can be specified on the command
  // line. This is intended to be used to pass generator specific parameters.
  // It is empty if no parameter was given. ParseGeneratorParameter (below),
  // can be used to accept multiple parameters within the single parameter
  // command line flag.
  //
  // Returns true if successful.  Otherwise, sets *error to a description of
  // the problem (e.g. "invalid parameter") and returns false.
  bool Generate(const FileDescriptor* file,
                const std::string& parameter,
                GeneratorContext* context,
                std::string* error) const override {
    
    try {
      auto file_name = path(file->name()).stem().string();

      {
        auto header_data = generate_header(file);

        std::unique_ptr<ZeroCopyOutputStream> header_dst{context->Open(file_name + header_extension)};
        CodedOutputStream header_out(header_dst.get());
        header_out.WriteRaw(header_data.data(), header_data.size());
      }

      {
        auto src_data = generate_source(file);
        std::unique_ptr<ZeroCopyOutputStream> src_dst{context->Open(file_name + source_extension)};
        CodedOutputStream src_out(src_dst.get());
        src_out.WriteRaw(src_data.data(), src_data.size());
      }
    }
    catch(std::exception& e) {
      *error = e.what();
      return false;
    } 


    return true;
  }

};

int main(int argc, char* argv[]) {
  Generator generator;
  ::google::protobuf::compiler::PluginMain(argc, argv, &generator);
}