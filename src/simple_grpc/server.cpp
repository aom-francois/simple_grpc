#include "simple_grpc/server/server.h"

namespace simple_grpc {

namespace server {
  Config& Config::with_service(Service* service) {
    services_.push_back(service);
    return *this;
  }

  Config& Config::with_listening_port(std::string addr, std::shared_ptr<Credentials> creds) {
    ports_.push_back({std::move(addr), creds});
    return *this;
  }

  Server::Server(Config cfg) {
    impl_ = grpc_server_create(nullptr, nullptr);
  }

  Server::~Server() {
    cleanup_();
  }

  Server::Server(Server&& rhs) : impl_(rhs.impl_) {
    rhs.impl_ = nullptr;
  }

  Server& Server::operator=(Server&& rhs) { 
    cleanup_();
    impl_ = rhs.impl_; 
    rhs.impl_ = nullptr;

    return *this;
  }

  void Server::cleanup_() {
    if(impl_) {
      grpc_server_destroy(impl_);
    }
  }
}

}