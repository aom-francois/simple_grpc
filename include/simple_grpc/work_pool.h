#ifndef SIMPLE_GRPC_WORK_POOL_INCLUDED_H 
#define SIMPLE_GRPC_WORK_POOL_INCLUDED_H

#include "grpc/grpc.h"

namespace simple_grpc {
  class Work_pool {
    public:
      Work_pool(int min_threads, int max_threads) {}

    private:
      grpc_completion_queue* handle_;
  };
}
#endif