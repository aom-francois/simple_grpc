// This header is meant to be included by headers that were generated
// using the protoc plugin.
#ifndef SIMPLE_GRPC_GEN_SUPPORT_INCLUDED_H
#define SIMPLE_GRPC_GEN_SUPPORT_INCLUDED_H

#include "simple_grpc/client/channel.h"
#include "simple_grpc/server/service.h"
#include "simple_grpc/completion_queue.h"
#include "simple_grpc/future.h"
#include "simple_grpc/work_pool.h"

#endif