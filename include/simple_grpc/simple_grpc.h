#ifndef SIMPLE_GRPC_LIB_H_INCLUDED
#define SIMPLE_GRPC_LIB_H_INCLUDED

#include "simple_grpc/completion_queue.h"
#include "simple_grpc/environment.h"
#include "simple_grpc/future.h"
#include "simple_grpc/work_pool.h"

#include "simple_grpc/client/unsecure_channel.h"


#include "simple_grpc/server/server.h"
#include "simple_grpc/server/service.h"

#endif