#ifndef SIMPLE_GRPC_EXPECTED_INCLUDED_H 
#define SIMPLE_GRPC_EXPECTED_INCLUDED_H

#include <variant>

namespace async_grpc {
  template<typename T, typename E>
  class Expected {
    std::variant<T, E> data_;
  public:
    bool has_value() const {
      return data_.index() == 0;
    }

    operator bool() const {
      return has_value();
    }
  };
}
#endif