FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
  build-essential autoconf git pkg-config \
  automake libtool curl make g++ unzip cmake \
  && apt-get clean

# install protobuf first, then grpc
ENV GRPC_RELEASE_TAG v1.10.x
RUN git clone -b ${GRPC_RELEASE_TAG} https://github.com/grpc/grpc /var/local/git/grpc && \
    cd /var/local/git/grpc && \
    git submodule update --init && \
    echo "--- installing protobuf ---" && \
    cd third_party/protobuf && \
    ./autogen.sh && ./configure --enable-shared && \
    make -j$(nproc) && make -j$(nproc) check && make install && make clean && ldconfig && \
    echo "--- installing grpc ---" && \
    cd /var/local/git/grpc && \
    make -j$(nproc) && make install && make clean && ldconfig

RUN apt-get update && apt-get install -y g++-8 && apt-get clean
RUN update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 20

