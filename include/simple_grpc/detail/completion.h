#ifndef ASYNC_GRPC_DETAIL_COMPLETION_INCLUDED_H
#define ASYNC_GRPC_DETAIL_COMPLETION_INCLUDED_H

namespace async_grpc {
  // GRPC completion queues provide a single "tag" pointer. We'll make sure every 
  // single one of them inherits from this class.
  namespace detail { 

    class Completion {
    public:
      virtual ~Completion() {}
      virtual void exec() = 0;
    };
  }
}
#endif