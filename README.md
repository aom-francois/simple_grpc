# simple_grpc

This project is an attempt to create a wrapper around GRPC to make
writing asynchronous servers as straightforward as possible. It may not
be quite as optimal as a fully-customized server working on raw completion 
queues, but every effort is being made to get as close as possible while
maintaining as sane and modern an API as possible.

## Design philosophy

### Minimal API surface

### Asynchronous by default


## Futures

simple_grpc relies on futures for managing chained asynchronous operations.

Synchronous call
```
{
  Future<MyReply> res_f = my_stub->MyMethod(MyRequest{});

  // This will throw an exception if the rpc fails
  MyReply res = res_f.get();
}

// As a one-liner
{
  auto res = my_stub->MyMethod(MyRequest{}).get();
}
```

Asynchronous handling
```
my_stub->MyMethod(MyRequest{}).then_or([](expected<MyResponse> r){
  std::cerr << "reply received: " << r.DebugString() << "\n";
}, [](Status s) {
  std::cerr << "something went wrong! " << s << "\n";
});
```



Delayed call
```
Future<MyRequest> f = ...;
Future<MyResponse> res = my_stub->MyMethod(f); // MyMethod will be sent once f is ready
```


// Asynchronous handling with error callback
```
my_stub->MyMethod(MyRequest{}).then_or([](MyResponse r){
  std::cerr << r.DebugString() << "\n";
}, [](grpc::Status s){
  std::cerr << "error: " <<s.DebugString() << "\n";
});
```

